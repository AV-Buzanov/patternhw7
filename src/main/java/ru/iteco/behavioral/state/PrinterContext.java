package ru.iteco.behavioral.state;

import java.math.BigDecimal;

/**
 * ConnectionContext.
 *
 * @author Ilya_Sukhachev
 */
public class PrinterContext {

    public static final BigDecimal VALUE = BigDecimal.valueOf(3);
    private String url;
    private State state;
    private Source source;
    private BigDecimal money;

    public PrinterContext(BigDecimal money) {
        this.money = money;
        this.state = new InitState();
    }

    public void setSource(Source source) {
        this.source = source;
        state.setSource(this);
    }

    public void setUrl(String url) {
        this.url = url;
        state.setPath(this);
    }

    public void print() {
        state.print(this);
    }

    public void close() {
        state.close(this);
    }

    public String getUrl() {
        return url;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Source getSource() {
        return source;
    }


    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }
}
