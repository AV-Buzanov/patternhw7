package ru.iteco.behavioral.state;

import java.math.BigDecimal;
import java.util.Scanner;

/**
 * Main.
 *
 * @author Ilya_Sukhachev
 */
public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Внесите средства");
        try {
            PrinterContext printerContext = new PrinterContext(BigDecimal.valueOf(scanner.nextLong()));
            System.out.println("Выберете источник данных");
            printerContext.setSource(Source.valueOf(scanner.next()));
            while (!(printerContext.getState() instanceof CloseState)) {
                System.out.println("Введите путь к файлу");
                printerContext.setUrl(scanner.next());
                printerContext.print();
                System.out.println("Продолжить?");
                if (scanner.next().equals("n")) {
                    printerContext.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
