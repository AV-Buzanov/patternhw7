package ru.iteco.behavioral.state;

/**
 * CloseState.
 *
 * @author Ilya_Sukhachev
 */
public class SourceChosenState extends StateBase {

    @Override
    public void setSource(PrinterContext context) {
        throw new RuntimeException("источник данных уже выбран");
    }

    @Override
    public void setPath(PrinterContext context) {
        System.out.println("Выбран файл:" + context.getUrl());
        context.setState(new PathChosenState());
    }

    @Override
    public void print(PrinterContext context) {
        throw new RuntimeException("Не выбран файл");
    }
}
