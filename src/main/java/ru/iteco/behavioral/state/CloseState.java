package ru.iteco.behavioral.state;

/**
 * CloseState.
 *
 * @author Ilya_Sukhachev
 */
public class CloseState extends StateBase {

    @Override
    public void setSource(PrinterContext context) {
        throw new RuntimeException("Connection is closed");
    }

    @Override
    public void setPath(PrinterContext context) {
        throw new RuntimeException("Connection is closed");
    }

    @Override
    public void print(PrinterContext context) {
        throw new RuntimeException("Connection is closed");
    }

    @Override
    public void close(PrinterContext context) {
        throw new RuntimeException("Connection is closed");
    }
}
