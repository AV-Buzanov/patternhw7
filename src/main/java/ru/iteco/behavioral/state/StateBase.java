package ru.iteco.behavioral.state;

/**
 * StateBase.
 *
 * @author Ilya_Sukhachev
 */
public abstract class StateBase implements State {

    @Override
    public void close(PrinterContext context) {
        System.out.printf("Печать завершена, заберите сдачу: %s", context.getMoney());
        System.out.println();
        context.setState(new CloseState());
    }
}
