package ru.iteco.behavioral.state;

/**
 * State.
 *
 * @author Ilya_Sukhachev
 */
public interface State {

    void setSource(PrinterContext context);

    void setPath(PrinterContext context);

    void print(PrinterContext context);

    void close(PrinterContext context);
}
