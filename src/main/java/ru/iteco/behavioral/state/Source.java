package ru.iteco.behavioral.state;

public enum Source {
    FLASH,
    WIFI,
    BLUETOOTH
}
