package ru.iteco.behavioral.state;

/**
 * InitState.
 *
 * @author Ilya_Sukhachev
 */
public class InitState extends StateBase {

    @Override
    public void setSource(PrinterContext context) {
        System.out.println("Выбран источник данных:" + context.getSource());
        context.setState(new SourceChosenState());
    }

    @Override
    public void setPath(PrinterContext context) {
        throw new RuntimeException("Не выбран источник данных");
    }

    @Override
    public void print(PrinterContext context) {
        throw new RuntimeException("Не выбран источник данных и путь к файлу");
    }
}
