package ru.iteco.behavioral.state;

/**
 * CloseState.
 *
 * @author Ilya_Sukhachev
 */
public class PathChosenState extends StateBase {

    @Override
    public void setSource(PrinterContext context) {
        throw new RuntimeException("источник данных уже выбран");
    }

    @Override
    public void setPath(PrinterContext context) {
        System.out.println("Выбран другой файл:" + context.getUrl());
    }

    @Override
    public void print(PrinterContext context) {
        if (context.getMoney().compareTo(PrinterContext.VALUE) < 0) {
            System.out.println("Недостаточно средств");
            context.setState(new CloseState());
            return;
        }
        System.out.printf("Распечатан файл %s из %s:", context.getUrl(), context.getSource());
        System.out.println();
        context.setMoney(context.getMoney().subtract(PrinterContext.VALUE));
        context.setState(new SourceChosenState());
    }
}
